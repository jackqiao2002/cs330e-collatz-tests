#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "50 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 50)
        self.assertEqual(j, 100)

    def test_read_3(self):
        s = "5 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 5)

    def test_read_4(self):
        s = "5 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 5)

    def test_read_5(self):
        s = "15893 500000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 15893)
        self.assertEqual(j, 500000)

    def test_read_6(self):
        s = "341221 12342\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 341221)
        self.assertEqual(j, 12342)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    def test_eval_6(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_eval_7(self):
        v = collatz_eval(2412, 12340)
        self.assertEqual(v, 268)

    def test_eval_8(self):
        v = collatz_eval(341221, 12342)
        self.assertEqual(v, 443)

    def test_eval_9(self):
        v = collatz_eval(302, 23412)
        self.assertEqual(v, 279)

    def test_eval_10(self):
        v = collatz_eval(1, 649)
        self.assertEqual(v, 145)
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 5, 5, 6)
        self.assertEqual(w.getvalue(), "5 5 6\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO(
            "80 10000\n5 5\n302 23412\n341221 12342\n2412 12340\n999999 999999")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "80 10000 262\n5 5 6\n302 23412 279\n341221 12342 443\n2412 12340 268\n999999 999999 259\n")

    def test_solve_3(self):
        r = StringIO("1 1\n10 10\n900 3000\n5000 9000\n30 30")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n10 10 7\n900 3000 217\n5000 9000 262\n30 30 19\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
